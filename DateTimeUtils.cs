﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Net_Neuralab_Utilities
{
    public class DateTimeUtils
    {
        //Check if given year is a leap year//////////////////////////////////////////////////////////////////////

        public static bool IsLeapYear(int year)
        {
            if (year % 4 != 0)
            {
                return false;
            }
            if (year % 100 == 0)
            {
                return (year % 400 == 0);
            }
            return true;
        }


        //Method generates string for online date-dependant filenaming (usefull if you want to save large number of files online ... i.e. images, pdfs... that can be public)////////////////////////////////////////////

        public static string generateStringFromDate()
        {
            DateTime dt = DateTime.Now;

            string dts = dt.Year.ToString() + "x" + dt.Month.ToString() + "x" + dt.Day.ToString() + "x" + dt.Hour.ToString() + "x" + dt.Minute.ToString() + "x" + dt.Second.ToString() + "x" + dt.Millisecond.ToString();

            return dts;
        }



        //UNIX time handlers // TO and FROM///////////////////////////////////////////////////////////////////////

        public static DateTime getDetTimeFromUNIXtime(int numberOfSeconds)
        {
            DateTime basicDate = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            DateTime newDate = basicDate.AddSeconds(numberOfSeconds);

            return newDate;
        }

        public static int getUNIXtimeFromDateTime(DateTime dateTime)
        {
            DateTime basicDate = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            DateTime newDate = dateTime;

            TimeSpan ts = newDate.Subtract(basicDate);


            return ts.Days * 24 * 60 * 60;
        }
        ///GetMonthNameCro prima datum i vrača polje gdje je prvi element dan, drugi hrvatski naziv danog mjeseca i treći godina
        public static ArrayList GetMonthNameCro(DateTime inpDate)
        {
            var rez = new ArrayList();
            rez.Add(inpDate.Day.ToString());
            switch (int.Parse(inpDate.Month.ToString()))
            {
                case 1: rez.Add("siječanj"); break;
                case 2: rez.Add("veljača"); break;
                case 3: rez.Add("ožujak"); break;
                case 4: rez.Add("travanj"); break;
                case 5: rez.Add("svibanj"); break;
                case 6: rez.Add("lipanj"); break;
                case 7: rez.Add("srpanj"); break;
                case 8: rez.Add("kolovoz"); break;
                case 9: rez.Add("rujan"); break;
                case 10: rez.Add("listopad"); break;
                case 11: rez.Add("studeni"); break;
                case 12: rez.Add("prosinac"); break;
            }
            rez.Add(inpDate.Year.ToString());
            return rez;

        }

        ///iz Neuralab.dll vrača za redni broj dana hrvatski naziv
        public static string getDayNameCro(DateTime dt)
        {
            ArrayList list = new ArrayList();
            list.Add("Pondjeljak");
            list.Add("Utorak");
            list.Add("Srijeda");
            list.Add("Četvrtak");
            list.Add("Petak");
            list.Add("Subota");
            list.Add("Nedjelja");
            DayOfWeek dayOfWeek = dt.DayOfWeek;
            return "";
        }

    }
}
